FROM maven:3.8.6-eclipse-temurin-17 as builder
WORKDIR /app

COPY . /app
RUN mvn dependency:go-offline
RUN mvn package -DskipTests

FROM openjdk:17
WORKDIR /app
EXPOSE 8092

COPY --from=builder /app/target/agitex.jar /app/agitex.jar

ENTRYPOINT ["java","-jar","agitex.jar"]