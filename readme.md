# AGITEX TEST

### Description
Traitement de fichier de type *XML* | *TXT* | *JSON* | *CSV* | *XLSX*

### Exécution
> Les fichiers de test sont dans [data/files](data/files)

#### Cloner le projet sur gitlab

[https://gitlab.com/wendkouny-projects/agitex_test](https://gitlab.com/wendkouny-projects/agitex_test)

#### Builder l'image docker (faire attention au caractère `.` à la fin)
```
docker build -t agitext_test .
```
#### Demarrer un container à partir de l'image buider
```
docker run -p 8092:8092 agitext_test
```
#### Tester avec un navigateur ou utiliser soit Postman ou Insomnia
[http://localhost:8092/swagger-ui/index.html](http://localhost:8092/swagger-ui/index.html)

#### Acces à la base de données à partir du navigateur
[http://localhost:8092/h2-console](http://localhost:8092/h2-console)
