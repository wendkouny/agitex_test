package test.projects.agitex.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import test.projects.agitex.dto.CustomerDto;
import test.projects.agitex.dto.CustomerStats;
import test.projects.agitex.repository.CustomerRepository;
import test.projects.agitex.service.CustomerService;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/customers")
@RequiredArgsConstructor
@Tags(@Tag(name = "Customers", description = "Customers management"))
public class CustomerController {
    private final CustomerService customerService;

    /**
     * GET /:partkey : get all customers.
     *
     * @param partkey
     * @return {@link List< test.projects.agitex.dto.CustomerDto >}
     */
    @GetMapping("/{partkey}")
    @Operation(summary = "Fetch all customers")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "204", description = "${swagger.http-status.204}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<CustomerDto>> getAllUsers(@PathVariable final String partkey) {
        return new ResponseEntity<>(customerService.findAllCustomerByPartner(partkey), HttpStatus.OK);
    }

    /**
     * GET /stats/:partkey : get customer.
     *
     * @param partkey
     * @return {@link List< test.projects.agitex.dto.CustomerDto >}
     */
    @GetMapping("/stats/{partkey}")
    @Operation(summary = "Get customer")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "${swagger.http-status.200}"),
            @ApiResponse(responseCode = "404", description = "${swagger.http-status.404}"),
            @ApiResponse(responseCode = "500", description = "${swagger.http-status.500}")
    })
    public ResponseEntity<List<CustomerStats>> findUser(@PathVariable final String partkey) {
        return ResponseEntity.ok(customerService.getAvgSalaryGroupedByProfession(partkey));
    }

}
