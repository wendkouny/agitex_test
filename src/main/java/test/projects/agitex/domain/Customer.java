package test.projects.agitex.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@Entity
@Builder
@Table(name = "agitex_customers")
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class Customer implements Serializable {

    @Serial
    @Builder.Default
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq_generator")
    @SequenceGenerator(name = "customer_seq_generator", sequenceName = "agitex_customer_sequence",
            initialValue = 1000, allocationSize = 1)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "first_name", length = 100, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 100, nullable = false)
    private String lastName;

    @Column(name = "age", nullable = false)
    private int age;

    @Column(name = "profession", length = 50, nullable = false)
    private String profession;

    @Column(name = "salary", nullable = false)
    private int salary;

    @Size(min = 10, max = 10)
    @Column(name = "partner_key", length = 10, nullable = false)
    private String partnerKey;
}
