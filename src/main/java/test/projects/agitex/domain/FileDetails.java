package test.projects.agitex.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import test.projects.agitex.domain.enums.FileStatus;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@Table(name = "agitex_file_details")
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class FileDetails implements Serializable {

    @Serial
    @Builder.Default
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @NotNull
    @Column(name = "file_sequence", nullable = false)
    private int sequence;

    @NotNull
    @Size(min = 10, max = 10)
    @Column(name = "partner_key_", length = 10, nullable = false)
    private String partnerKey;

    @NotNull
    @Column(name = "file_name", nullable = false)
    private String fileName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "file_status", nullable = false)
    private FileStatus status;

    @NotNull
    @Column(name = "process_time", nullable = false)
    private LocalDateTime processTime;

}
