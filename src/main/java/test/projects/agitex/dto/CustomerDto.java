package test.projects.agitex.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
public class CustomerDto implements Serializable {

    private Long id;

    @NotEmpty(message = "FirstName field cannot be empty")
    @NotNull(message = "FirstName field is mandatory")
    private String firstName;

    @Size(min = 2, max = 25)
    @NotEmpty(message = "LastName field cannot be empty")
    @NotNull(message = "LastName field is mandatory")
    private String lastName;

    @NotNull(message = "Age field is mandatory")
    private int age;

    @Email
    @Size(max = 50)
    @NotEmpty(message = "Profession field cannot be empty")
    @NotNull(message = "Profession field is mandatory")
    private String profession;

    @NotNull(message = "Salary field is mandatory")
    private int salary;

    @Size(min = 10, max = 10)
    private String partnerKey;
}
