package test.projects.agitex.dto;

public interface CustomerStats {
    String getProfession();

    int getSalary();
}
