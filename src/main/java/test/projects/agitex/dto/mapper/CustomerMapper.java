package test.projects.agitex.dto.mapper;

import org.springframework.stereotype.Component;
import test.projects.agitex.domain.Customer;
import test.projects.agitex.dto.CustomerDto;
import java.util.List;

@Component
public class CustomerMapper {
    public CustomerDto toDto(Customer customer) {
        return CustomerDto.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .age(customer.getAge())
                .profession(customer.getProfession())
                .salary(customer.getSalary())
                .partnerKey(customer.getPartnerKey())
                .build();
    }

    public Customer toEntity(CustomerDto customerDto) {
        return Customer.builder()
                .id(customerDto.getId())
                .firstName(customerDto.getFirstName())
                .lastName(customerDto.getLastName())
                .age(customerDto.getAge())
                .profession(customerDto.getProfession())
                .salary(customerDto.getSalary())
                .partnerKey(customerDto.getPartnerKey())
                .build();
    }

    public List<CustomerDto> toDtos(List<Customer> customers) {
        return customers.stream().map(this::toDto).toList();
    }

    public List<Customer> toEntities(List<CustomerDto> customerDtos) {
        return customerDtos.stream().map(this::toEntity).toList();
    }
}
