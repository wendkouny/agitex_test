package test.projects.agitex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import test.projects.agitex.domain.Customer;
import test.projects.agitex.dto.CustomerStats;
import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    List<Customer> findAllByPartnerKey(String partKey);

    @Query(value = "select c.profession as profession, avg(c.salary) as salary" +
            " from agitex_customers c where c.partner_key=:partKey group by c.profession",
            nativeQuery = true)
    List<CustomerStats> getCustomerStatsByPartnerKey(@Param("partKey") String partKey);
}
