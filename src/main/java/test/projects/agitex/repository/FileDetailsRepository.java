package test.projects.agitex.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import test.projects.agitex.domain.Customer;
import test.projects.agitex.domain.FileDetails;
import java.util.UUID;

public interface FileDetailsRepository extends JpaRepository<FileDetails, UUID> {
    FileDetails findTopByFileName(String fileName);
}
