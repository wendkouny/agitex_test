package test.projects.agitex.service;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import test.projects.agitex.domain.Customer;
import test.projects.agitex.dto.CustomerDto;
import test.projects.agitex.dto.CustomerStats;
import test.projects.agitex.dto.mapper.CustomerMapper;
import test.projects.agitex.repository.CustomerRepository;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerMapper mapper;
    private final CustomerRepository customerRepository;

    /**
     * Save list of Customer.
     *
     * @param customerDtos {@link test.projects.agitex.dto.CustomerDto}
     * @return created customer object
     */
    public List<CustomerDto> saveCustomers(final List<CustomerDto> customerDtos) {
        if (customerDtos.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "Customer list cannot be empty.");
        }
        List<Customer> savedCustomers = customerRepository.saveAll(mapper.toEntities(customerDtos));

        return mapper.toDtos(savedCustomers);
    }

    /**
     * Fetch all customer stored in DB.
     *
     * @return list of {@link test.projects.agitex.dto.CustomerDto}
     */
    public List<CustomerDto> findAllCustomerByPartner(final String partKey) {
        List<Customer> customers = customerRepository.findAllByPartnerKey(partKey);
        if (customers.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "No data found, Please create at least one customer before.");
        }
        return mapper.toDtos(customers);
    }

    /**
     * Compute salary AVG by profession.
     *
     * @return list of {@link test.projects.agitex.dto.CustomerDto}
     */
    public List<CustomerStats> getAvgSalaryGroupedByProfession(final String partKey) {
        return customerRepository.getCustomerStatsByPartnerKey(partKey);
    }

}
