package test.projects.agitex.service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import test.projects.agitex.domain.FileDetails;
import test.projects.agitex.domain.enums.FileStatus;
import test.projects.agitex.dto.CustomerDto;
import test.projects.agitex.exceptions.BusinessException;
import test.projects.agitex.repository.FileDetailsRepository;
import test.projects.agitex.service.data.IProcessData;
import test.projects.agitex.utils.FileAccessUtils;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
@Service
public class FileProcessService {

    @Autowired
    private FileAccessUtils fileAccessUtils;

    @Autowired
    private FileDetailsRepository fileDetailsRepository;

    @Autowired
    private CustomerService customerService;

    @Value("${directory.working}")
    String workingDirectory;

    @Value("${directory.rejecting}")
    String rejectingDirectory;

    /**
     * Create directories if not exists.
     */
    @PostConstruct
    protected void createDirectoriesIfAbsent() {
        File workingDir = new File(workingDirectory);
        File rejectingDir = new File(rejectingDirectory);
        if (!workingDir.exists()) {
            boolean dirCreated = workingDir.mkdirs();
            assert (dirCreated);
        }

        if (!rejectingDir.exists()) {
            boolean dirCreated = rejectingDir.mkdirs();
            assert (dirCreated);
        }
    }

    /**
     * Cron to lookup working directory et process files.
     */
    @Scheduled(cron = "${lookup.file.cron}")
    public void lookupDirectory() {
        log.debug("******* Starting files processing *******");

        try {
            this.fileAccessUtils.listFiles(this.workingDirectory).forEach(f -> {
                try {
                    this.processFile(f);
                } catch (BusinessException e) {
                    log.debug("Error while processing file.");
                }
            });
        } catch (BusinessException e) {
            log.warn("Error while looking up the directory.");
        }
    }


    /**
     * Process file and save data in DB.
     *
     * @param file
     * @throws BusinessException
     */
    protected void processFile(File file) throws BusinessException {
        String[] tokens = this.fileAccessUtils.getTokens(file.getName());
        FileDetails fileDetails = getFileDetails(file, tokens);

        if (this.fileAccessUtils.isSupported(file.getName())) {
            try {
                this.customerService.saveCustomers(this.getCustomers(file, tokens[4], tokens[1]));

                fileAccessUtils.changeFileStatus(fileDetails, FileStatus.DONE);
                fileDetailsRepository.save(fileDetails);

                fileAccessUtils.delete(file);
            } catch (IOException e) {
                this.rejectFile(file, fileDetails);
                throw new RuntimeException(e);
            }
        } else {
            this.rejectFile(file, fileDetails);
            throw new BusinessException();
        }
    }

    /**
     * Reject file if any error occurred while processing.
     *
     * @param file
     * @param fileDetails
     */
    private void rejectFile(File file, FileDetails fileDetails) {
        String newFilePath = rejectingDirectory + File.separator + file.getName();
        fileAccessUtils.moveFile(file, newFilePath);

        fileAccessUtils.changeFileStatus(fileDetails, FileStatus.REJECTED);
        fileDetailsRepository.save(fileDetails);
    }

    /**
     * Create FileDetails Instance.
     *
     * @param file
     * @param tokens
     * @return {@link FileDetails}
     */
    private FileDetails getFileDetails(File file, String[] tokens) {
        FileDetails fileDetails = fileDetailsRepository.findTopByFileName(file.getName());

        String partnerKey = tokens[1];
        int sequence = Integer.parseInt(tokens[3]);

        if (Objects.isNull(fileDetails)) {
            fileDetails = FileDetails.builder()
                    .uuid(UUID.randomUUID())
                    .fileName(file.getName())
                    .partnerKey(partnerKey)
                    .sequence(sequence)
                    .processTime(LocalDateTime.now())
                    .status(FileStatus.DOING)
                    .build();

            fileDetails = fileDetailsRepository.save(fileDetails);
        }

        return fileDetails;
    }

    /**
     * Get list of customer.
     *
     * @param file
     * @param extension
     * @param partKey
     * @return {@link List<CustomerDto>}
     * @throws IOException
     */
    List<CustomerDto> getCustomers(File file, String extension, String partKey) throws IOException {
        ClassLoader classLoader = IProcessData.class.getClassLoader();
        String packageName = IProcessData.class.getPackageName();

        try {
            Class<?> processDataInstance = classLoader.loadClass(packageName + ".Process" + StringUtils.capitalize(extension));
            IProcessData processData = (IProcessData) processDataInstance.getDeclaredConstructor().newInstance();

            return processData.readValues(file, partKey);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException |
                 InvocationTargetException e) {
            throw new IllegalStateException("Data process class has not been found for extension: " + extension, e);
        }
    }
}
