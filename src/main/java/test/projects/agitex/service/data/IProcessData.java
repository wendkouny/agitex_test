package test.projects.agitex.service.data;

import test.projects.agitex.dto.CustomerDto;
import java.io.File;
import java.io.IOException;
import java.util.List;

public interface IProcessData {
    List<CustomerDto> readValues(File file, String partKey) throws IOException;
}
