package test.projects.agitex.service.data;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import lombok.extern.slf4j.Slf4j;
import test.projects.agitex.dto.CustomerDto;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ProcessCsv implements IProcessData {

    /**
     * Read CSV file and build customer list.
     *
     * @param file
     * @param partKey
     * @return {@link List<CustomerDto>}
     */
    @Override
    public List<CustomerDto> readValues(File file, String partKey) {

        log.debug("============== ProcessCsv =============");

        try (CSVReader csvReader = new CSVReader(new FileReader(file));) {
            List<CustomerDto> customerDtos = new ArrayList<>();
            String[] values;

            while ((values = csvReader.readNext()) != null) {
                customerDtos.add(CustomerDto.builder()
                        .lastName(values[0])
                        .firstName(values[1])
                        .age(Integer.parseInt(values[2].trim()))
                        .profession(values[3])
                        .salary(Integer.parseInt(values[4].trim()))
                        .partnerKey(partKey)
                        .build());
            }

            return customerDtos;
        } catch (IOException | CsvValidationException e) {
            throw new RuntimeException(e);
        }
    }
}
