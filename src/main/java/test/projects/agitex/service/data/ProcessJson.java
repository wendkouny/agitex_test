package test.projects.agitex.service.data;

import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import test.projects.agitex.dto.CustomerDto;
import test.projects.agitex.utils.JsonTools;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
public class ProcessJson implements IProcessData {

    /**
     * Read JSON file and build customer list.
     *
     * @param file
     * @param partKey
     * @return {@link List<CustomerDto>}
     */
    @Override
    public List<CustomerDto> readValues(File file, String partKey) {
        log.debug("============== ProcessJson =============");

        try {
            List<CustomerDto> customerDtos = JsonTools.DEFAULT_OBJECT_MAPPER.readValue(file, new TypeReference<>() {
            });

            return customerDtos.stream().peek(c -> c.setPartnerKey(partKey)).toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
