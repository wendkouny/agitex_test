package test.projects.agitex.service.data;

import lombok.extern.slf4j.Slf4j;
import test.projects.agitex.dto.CustomerDto;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ProcessTxt implements IProcessData {

    /**
     * Read TXT file and build customer list.
     *
     * @param file
     * @param partKey
     * @return {@link List<CustomerDto>}
     */
    @Override
    public List<CustomerDto> readValues(File file, String partKey) {
        log.debug("============== ProcessTxt =============");

        try (RandomAccessFile accessFile = new RandomAccessFile(file, "r")) {
            String line;
            List<CustomerDto> customerDtos = new ArrayList<>();

            while ((line = accessFile.readLine()) != null) {
                String[] tokens = line.split(",");

                customerDtos.add(CustomerDto.builder()
                        .lastName(tokens[0])
                        .firstName(tokens[1])
                        .age(Integer.parseInt(tokens[2].trim()))
                        .profession(tokens[3])
                        .salary(Integer.parseInt(tokens[4].trim()))
                        .partnerKey(partKey)
                        .build());
            }

            return customerDtos;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
