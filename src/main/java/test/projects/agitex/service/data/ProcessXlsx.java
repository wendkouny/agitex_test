package test.projects.agitex.service.data;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.dhatim.fastexcel.reader.ReadableWorkbook;
import org.dhatim.fastexcel.reader.Row;
import org.dhatim.fastexcel.reader.Sheet;
import test.projects.agitex.dto.CustomerDto;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ProcessXlsx implements IProcessData {

    /**
     * Read XLSX file and build customer list.
     *
     * @param file
     * @param partKey
     * @return {@link List<CustomerDto>}
     */
    @Override
    public List<CustomerDto> readValues(File file, String partKey) {
        List<CustomerDto> customerDtos = new ArrayList<>();
        log.debug("============== ProcessXlsx =============");

        try (ReadableWorkbook workbook = new ReadableWorkbook(file)) {
            Sheet sheet = workbook.getFirstSheet();

            for (Row row : sheet.read()) {
                if (row.getRowNum() == 1) {
                    continue;
                }
                if (!StringUtils.isEmpty(row.getCellText(0))) {
                    customerDtos.add(CustomerDto.builder()
                            .lastName(row.getCellText(0))
                            .firstName(row.getCellText(1))
                            .age(Integer.parseInt(row.getCellText(2)))
                            .profession(row.getCellText(3))
                            .salary(Integer.parseInt(row.getCellText(4)))
                            .partnerKey(partKey)
                            .build());
                }
            }

            return customerDtos;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
