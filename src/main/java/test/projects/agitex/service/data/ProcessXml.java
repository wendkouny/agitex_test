package test.projects.agitex.service.data;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import test.projects.agitex.dto.CustomerDto;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Slf4j
public class ProcessXml implements IProcessData {

    /**
     * Read XML file and build customer list.
     *
     * @param file
     * @param partKey
     * @return {@link List<CustomerDto>}
     */
    @Override
    public List<CustomerDto> readValues(File file, String partKey) {
        log.debug("============== ProcessXml =============");
        XmlMapper xmlMapper = new XmlMapper();

        try {
            List<CustomerDto> customerDtos = xmlMapper.readValue(file, new TypeReference<>() {
            });

            return customerDtos.stream().peek(c -> c.setPartnerKey(partKey)).toList();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
