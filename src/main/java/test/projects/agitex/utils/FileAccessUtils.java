package test.projects.agitex.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;
import test.projects.agitex.domain.FileDetails;
import test.projects.agitex.domain.enums.FileStatus;
import test.projects.agitex.exceptions.BusinessException;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Utils for accessing files and directory
 */

@Slf4j
@Component
public class FileAccessUtils {

    private static final String TEST_FILES_PATTERN = "(CUST_BF[0-9]{8}_[0-9]{8}_[0-9]{4})\\.(json|csv|xlsx|xml|txt)$";

    interface FileMover {
        void move(File srcPath, File destPath) throws IOException;
    }

    FileMover fileMover = FileUtils::moveFile;

    interface FileDeleter {
        void delete(Path toDeletePath) throws IOException;
    }

    FileDeleter fileDeleter = Files::delete;

    /**
     * Process directory to exclude files which does not respect pattern.
     *
     * @param dir              le directory
     * @param exclusionPattern regex
     * @return files list that can be processed
     */
    public List<File> listFiles(String dir, Pattern exclusionPattern) throws BusinessException {
        File directory = new File(dir);
        if (directory.exists()) {
            return Arrays.asList(Objects.requireNonNull(new File(dir).listFiles(f -> !exclusionPattern.matcher(f.getName()).matches())));
        } else {
            throw new BusinessException("Directory not found");
        }
    }

    /**
     * Lookup files in the directory.
     *
     * @param dir le directory
     * @return files in the directory
     */
    public List<File> listFiles(String dir) throws BusinessException {
        File directory = new File(dir);
        if (directory.exists()) {
            return Arrays.asList(Objects.requireNonNull(directory.listFiles(f -> !f.isDirectory()
                    && !Pattern.compile("\\..*").matcher(f.getName()).matches())));
        } else {
            throw new BusinessException();
        }
    }

    /**
     * Check if file is in valid format.
     *
     * @param fileName
     * @return TRUE or FALSE
     */
    public boolean isSupported(String fileName) {
        return fileName.matches(TEST_FILES_PATTERN);
    }

    /**
     * Change file status (DOING|DONE|REJECTED).
     *
     * @param fileDetails
     * @param status
     * @return {@link FileDetails}
     */
    public FileDetails changeFileStatus(FileDetails fileDetails, FileStatus status) {
        fileDetails.setStatus(status);
        return fileDetails;
    }

    /**
     * Mov file to a destination directory according to process output.
     *
     * @param srcPath  src
     * @param destPath dest
     */
    public void moveFile(File srcPath, String destPath) {
        try {
            fileMover.move(srcPath, new File(destPath));
        } catch (FileExistsException e) {
            delete(new File(destPath));
            moveFile(srcPath, destPath);
        } catch (IOException e) {
            log.warn("Unable to move file " + srcPath.toPath() + " to " + new File(destPath).toPath());
        }
    }

    /**
     * Delete file.
     *
     * @param file
     */
    public void delete(File file) {
        try {
            fileDeleter.delete(file.toPath());
        } catch (IOException e) {
            log.debug(String.format("Unable to remove file %s : %s", file.toPath(), e.getMessage()));
        }
    }

    /**
     * Get file parts.
     *
     * @param fileName
     * @return {@link String[]}
     */
    public String[] getTokens(String fileName) {
        return fileName.split("[._]");
    }
}
