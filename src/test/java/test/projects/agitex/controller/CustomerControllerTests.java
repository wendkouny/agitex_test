package test.projects.agitex.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import test.projects.agitex.service.CustomerService;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTests {
    @InjectMocks
    CustomerController customerController;

    @Mock
    CustomerService customerService;

    @BeforeAll
    public static void init() {
        MockitoAnnotations.openMocks(CustomerControllerTests.class);
    }

}
