package test.projects.agitex.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import test.projects.agitex.dto.mapper.CustomerMapper;
import test.projects.agitex.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CustomerServiceTests {
    @InjectMocks
    CustomerService customerService;

    @Mock
    private CustomerMapper mapper;
    @Mock
    private CustomerRepository customerRepository;

}
